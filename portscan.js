self.onmessage = async function(event) {
    if (event.type === 'message') {
        console.log('Message from window:', ...event.data);
        const [ startPort, endPort, managerUsername, managerPassword ] = event.data;

        async function scanFn(port) {
            try {
                const res = await fetch(`http://127.0.0.1:${port}/api.json`, {
                    headers: {
                        Authorization: 'Basic ' + btoa(`${managerUsername}:${managerPassword}`)
                    },
                });
                return res.ok &&
                    res.headers.get('Content-Type') === 'application/json';
            } catch (err) {
                return false;
            }
        }

        const ports = [];
        for (let port = startPort; port <= endPort; port++) {
            const progress = (port - startPort) / (endPort - startPort);
            self.postMessage([ 'progress', progress ]);
            if (await scanFn(port)) {
                ports.push(port);
                break;
            }
        }
        const success = ports.length > 0;
        const port = ports[0];
        self.postMessage([ 'finish', success, port ]);
    }
};

function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms))
}