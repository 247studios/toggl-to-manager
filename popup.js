const toggl = new Toggl();
const clockify = new Clockify();

const managerCustomerCollectionGuid = 'ec37c11e-2b67-49c6-8a58-6eccb7dd75ee';
const managerEmployeeCollectionGuid = 'dadb7f95-a5dd-45c0-945d-6ad4ee28776e';
const managerBillableTimeCollectionGuid = '6bfb652c-11cb-46fa-9e5a-c5950ccbae15';
const managerPayslipCollectionGuid = '1d103fa7-6fc1-4951-811e-972968b842cc';

const credentialsTab = document.getElementById('credentials');
const togglApiKeyField = credentialsTab.querySelector('[name="togglApiKey"]');
const clockifyApiKeyField = credentialsTab.querySelector('[name="clockifyApiKey"]');
const timeTrackerFields = Array.from(credentialsTab.querySelectorAll('[name="timeTracker"]'));
const managerUrlField = credentialsTab.querySelector('[name="managerUrl"]');
const managerUsernameField = credentialsTab.querySelector('[name="managerUsername"]');
const managerPasswordField = credentialsTab.querySelector('[name="managerPassword"]');
const credentialsSubmit = document.getElementById('credentials-submit');

const timeTrackerNames = Array.from(document.querySelectorAll('.timetracker-name'));

const fetchNamespacesTab = document.getElementById('fetch-namespaces');
const fetchNamespacesTimeTrackerWorkspaces = document.getElementById('fetch-namespaces-timetracker-workspaces');
const fetchNamespacesManagerApp = document.getElementById('fetch-namespaces-manager-app');
const fetchNamespacesManagerAppProgress = document.getElementById('fetch-namespaces-manager-app-progress');
const fetchNamespacesManagerBusinesses = document.getElementById('fetch-namespaces-manager-businesses');
const fetchNamespacesSubmit = document.getElementById('fetch-namespaces-submit');
const fetchNamespacesError = document.getElementById('fetch-namespaces-error');

const chooseNamespacesTab = document.getElementById('choose-namespaces');
const timeTrackerWorkspaceIdInput = chooseNamespacesTab.querySelector('[name="timeTrackerWorkspaceId"]');
const managerBusinessIdInput = chooseNamespacesTab.querySelector('[name="managerBusinessId"]');
const chooseNamespacesBack = document.getElementById('choose-namespaces-back');

const fetchClientsTab = document.getElementById('fetch-clients');
const fetchClientsTimeTrackerClients = document.getElementById('fetch-clients-timetracker-clients');
const fetchClientsTimeTrackerUsers = document.getElementById('fetch-clients-timetracker-team');
const fetchClientsManagerCustomers = document.getElementById('fetch-clients-manager-customers');
const fetchClientsManagerEmployees = document.getElementById('fetch-clients-manager-employees');
const fetchClientsSubmit = document.getElementById('fetch-clients-submit');
const fetchClientsError = document.getElementById('fetch-clients-error');

const importReportTab = document.getElementById('import-report');
const timeTrackerClientIdInput = importReportTab.querySelector('[name="timeTrackerClientId"]');
const managerCustomerIdInput = importReportTab.querySelector('[name="managerCustomerId"]');
const timeTrackerUserIdInput = importReportTab.querySelector('[name="timeTrackerUserId"]');
const managerEmployeeIdInput = importReportTab.querySelector('[name="managerEmployeeId"]');
const reportStartDateInput = importReportTab.querySelector('[name="reportStartDate"]');
const reportEndDateInput = importReportTab.querySelector('[name="reportEndDate"]');
const importReportBack = document.getElementById('import-report-back');

const fetchReportTab = document.getElementById('fetch-report');
const fetchReportTimeTracker = document.getElementById('fetch-report-timetracker');
const fetchReportSubmit = document.getElementById('fetch-report-submit');
const fetchReportError = document.getElementById('fetch-report-error');

const generateTab = document.getElementById('generate');
const generatePreview = document.getElementById('generate-preview');
const generateSubmit = document.getElementById('generate-submit');
const generateBack = document.getElementById('generate-back');
const generateError = document.getElementById('generate-error');

const successTab = document.getElementById('success');
const successManagerEntity = document.getElementById('success-manager-entity');
const successTimeTrackerEntity = document.getElementById('success-timetracker-entity');
const successLink = document.getElementById('success-link');

let togglApiKey = null;
let clockifyApiKey = null;
let timeTracker = null;
let managerUrl = null;
let managerUsername = null;
let managerPassword = null;
let timeTrackerWorkspaces = null;
let managerBusinesses = null;
let timeTrackerClients = null;
let timeTrackerUsers = null;
let managerCustomers = null;
let managerEmployees = null;
let timeTrackerReport = null;
let managerBillableTime = null;
let managerPayslip = null;
let selections = {
  timeTracker: null,
  timeTrackerWorkspaceId: null,
  managerBusinessId: null,
  reportType: null, // 'client' or 'employee'
  timeTrackerClientId: null,
  managerCustomerId: null,
  timeTrackerUserId: null,
  managerEmployeeId: null,
  reportStartDate: null,
  reportEndDate: null,
}

let abortController;

chrome.storage.sync.get([
  'timeTracker',
  'togglApiKey',
  'clockifyApiKey',
  'managerUrl',
  'managerUsername',
  'managerPassword',
], function(data) {
  console.log('Retrieved credentials', data);
  const tt = data.timeTracker || selections.timeTracker || '';
  timeTrackerFields.forEach(el => {
    el.checked = el.value === tt;
  })
  togglApiKeyField.value = data.togglApiKey || togglApiKey || ''
  toggl.setApiKey(togglApiKeyField.value)
  clockifyApiKeyField.value = data.clockifyApiKey || clockifyApiKey || ''
  clockify.setApiKey(clockifyApiKeyField.value)
  managerUrlField.value = data.managerUrl || managerUrl || ''
  managerUsernameField.value = data.managerUsername || managerUsername || ''
  managerPasswordField.value = data.managerPassword || managerPassword || ''

  if (((togglApiKeyField.value && tt === 'Toggl') || (clockifyApiKeyField.value && tt === 'Clockify')) && managerUsernameField.value) {
    credentialsSubmit.click();
  }
});

credentialsTab.addEventListener('submit', async function(event) {
  event.preventDefault();
  setTab(1);
  togglApiKey = togglApiKeyField.value;
  toggl.setApiKey(togglApiKey)
  clockifyApiKey = clockifyApiKeyField.value;
  clockify.setApiKey(clockifyApiKey)
  managerUrl = managerUrlField.value;
  managerUsername = managerUsernameField.value;
  managerPassword = managerPasswordField.value;
  selections.timeTracker = timeTrackerFields.find(el => el.checked).value;
  switch (selections.timeTracker) {
    case 'Toggl':
      timeTracker = toggl;
      break;
      
    case 'Clockify':
      timeTracker = clockify;
      break;

    default:
      throw new Error();
  }
  timeTrackerNames.forEach(el => {
    el.innerText = selections.timeTracker
  })
  fetchNamespacesTimeTrackerWorkspaces.className = 'text-warning';
  fetchNamespacesManagerApp.style.display = 'none';
  fetchNamespacesManagerApp.className = 'text-warning';
  fetchNamespacesManagerBusinesses.className = 'text-warning';
  fetchNamespacesSubmit.innerText = 'Stop';
  fetchNamespacesError.innerText = '';
  timeTrackerWorkspaces = null;
  managerBusinesses = null;

  abortController = new AbortController()
  const signal = abortController.signal

  const timeTrackerWorkspacesResponse = await timeTracker.getWorkspaces({
    signal,
  });
  if (timeTrackerWorkspacesResponse.ok) {
    timeTrackerWorkspaces = timeTrackerWorkspacesResponse.workspaces();
    fetchNamespacesTimeTrackerWorkspaces.className = 'text-success';
    console.log(selections.timeTracker + ' Workspaces: OK');
  } else {
    fetchNamespacesTimeTrackerWorkspaces.className = 'text-danger';
    console.log(selections.timeTracker + ' Workspaces: NOT OK');
  }

  if (!managerUrl) {
    console.log('Port Scanning for Manager Desktop Edition');
    fetchNamespacesManagerApp.style.display = '';
    fetchNamespacesManagerAppProgress.innerText = '0';
    
    const workers = [];
    function stopAllWorkers() {
      workers.forEach(worker => worker.terminate());
    }
    signal.onabort = stopAllWorkers;

    const numberOfWorkers = 20;
    const portsToScanPerWorker = Math.floor(15535 / numberOfWorkers);
    
    try {
      managerUrl = await new Promise((resolve, reject) => {
        const workerProgress = Array.from(Array(numberOfWorkers)).map((_) => 0);
        let finishedWorkers = 0;
        let targetUrl;

        function handlePortScanResponse(workerIndex, event) {
          if (event.type === 'message') {
            console.log('Message from worker:', ...event.data);
            const type = event.data[0];
            if (type === 'progress') {
              const [ _, progress ] = event.data;
              workerProgress[workerIndex] = progress;
              fetchNamespacesManagerAppProgress.innerText = Math.round(100 * workerProgress
                  .reduce((t, p, a) => t + (p / workerProgress.length), 0))
            } else if (type === 'finish') {
              const [ _, success, port ] = event.data;
              finishedWorkers++;
              
              if (success) {
                fetchNamespacesManagerAppProgress.innerText = '100';
                targetUrl = `http://127.0.0.1:${port}`;
              }

              if (success || finishedWorkers === workers.length) {
                stopAllWorkers();
                if (targetUrl) {
                  resolve(targetUrl);
                } else {
                  reject(new Error('Scanned ports 50000 to 65535, Manager app was not found.'));
                }
              }
            }
          }
        };

        for (let i = 0; i < numberOfWorkers; i++) {
          const worker = new Worker(chrome.runtime.getURL('portscan.js'));
          worker.onmessage = handlePortScanResponse.bind(null, i);
          const startPort = 50000 + i * portsToScanPerWorker;
          const endPort = i < numberOfWorkers - 1
            ? 50000 - 1 + (i + 1) * portsToScanPerWorker
            : 65535;
          worker.postMessage([startPort, endPort, managerUsername, managerPassword]);
          workers.push(worker);
        }
      });
      fetchNamespacesManagerApp.className = 'text-success';
      fetchNamespacesManagerAppProgress.innerText = '100';
      console.log('Manager Port Scanning: OK');
    } catch (err) {
      fetchNamespacesManagerApp.className = 'text-danger';
      fetchNamespacesManagerBusinesses.className = 'text-danger';
      console.log('Manager Port Scanning: NOT OK');
      console.error(err);
      fetchNamespacesSubmit.innerText = 'Back';
      fetchNamespacesError.innerText = 'Verify that your Manager app is running locally.';
      return;
    }
  }

  const managerResponse = await fetch(`${managerUrl}/api.json`, {
    headers: {
      Authorization: 'Basic ' + btoa(`${managerUsername}:${managerPassword}`),
    },
    signal,
  });
  if (managerResponse.ok) {
    managerBusinesses = (await managerResponse.json())
      .map(business => ({
        id: business.Key,
        name: business.Name,
      }));
    fetchNamespacesManagerBusinesses.className = 'text-success';
    console.log('Manager Businesses: OK');
  } else {
    fetchNamespacesManagerBusinesses.className = 'text-danger';
    console.log('Manager Businesses: NOT OK');
  }

  if (timeTrackerWorkspacesResponse.ok && managerResponse.ok) {
    console.log(`${timeTrackerWorkspaces.length} ${selections.timeTracker} Workspaces`, timeTrackerWorkspaces);
    console.log(`${managerBusinesses.length} Manager Businesses`, managerBusinesses);
    populateSelectWithOptions(timeTrackerWorkspaceIdInput, timeTrackerWorkspaces);
    populateSelectWithOptions(managerBusinessIdInput, managerBusinesses);
    const storage = {
      timeTracker: selections.timeTracker,
      togglApiKey,
      clockifyApiKey,
      managerUrl,
      managerUsername,
      managerPassword,
    }
    chrome.storage.sync.set(storage, function() {
      console.log('Saved credentials', storage)
      setTab(2);
    });
  } else {
    fetchNamespacesSubmit.innerText = 'Back';
    fetchNamespacesError.innerText = 'Verify the credentials are correct and your computer has an internet connection.';
  }
});

chooseNamespacesTab.addEventListener('submit', async function(event) {
  event.preventDefault();
  setTab(3);
  const formData = new FormData(event.target);
  for (const [key, value] of formData) {
    selections[key] = value;
  }
  fetchClientsTimeTrackerClients.className = 'text-warning';
  fetchClientsTimeTrackerUsers.className = 'text-warning';
  fetchClientsManagerCustomers.className = 'text-warning';
  fetchClientsManagerEmployees.className = 'text-warning';
  fetchClientsSubmit.innerText = 'Stop';
  fetchClientsError.innerText = '';

  abortController = new AbortController()
  const signal = abortController.signal

  const timeTrackerClientsResponse = await timeTracker.getClients(selections.timeTrackerWorkspaceId, {
    signal,
  });
  if (timeTrackerClientsResponse.ok) {
    timeTrackerClients = timeTrackerClientsResponse.clients();
    fetchClientsTimeTrackerClients.className = 'text-success';
    console.log(selections.timeTracker + ' Clients: OK');
  } else {
    fetchClientsTimeTrackerClients.className = 'text-danger';
    console.log(selections.timeTracker + ' Clients: NOT OK');
  }

  const timeTrackerUsersResponse = await timeTracker.getUsers(selections.timeTrackerWorkspaceId, {
    signal,
  })
  if (timeTrackerUsersResponse.ok) {
    timeTrackerUsers = timeTrackerUsersResponse.users();
    fetchClientsTimeTrackerUsers.className = 'text-success';
    console.log(selections.timeTracker + ' Users: OK');
  } else {
    fetchClientsTimeTrackerUsers.className = 'text-danger';
    console.log(selections.timeTracker + ' Users: NOT OK');
  }

  const managerCustomersResponse = await fetch(`${managerUrl}/api/${selections.managerBusinessId}/${managerCustomerCollectionGuid}.json`, {
    headers: {
      Authorization: 'Basic ' + btoa(`${managerUsername}:${managerPassword}`)
    },
    signal,
  });
  if (managerCustomersResponse.ok) {
    managerCustomers = (await managerCustomersResponse.json())
      .map((customer) => ({
        id: customer.Key,
        name: customer.Name,
      }));
    fetchClientsManagerCustomers.className = 'text-success';
    console.log('Manager Customers: OK');
  } else {
    fetchClientsManagerCustomers.className = 'text-danger';
    console.log('Manager Customers: NOT OK');
  }

  const managerEmployeesResponse = await fetch(`${managerUrl}/api/${selections.managerBusinessId}/${managerEmployeeCollectionGuid}.json`, {
    headers: {
      Authorization: 'Basic ' + btoa(`${managerUsername}:${managerPassword}`)
    },
    signal,
  });
  if (managerEmployeesResponse.ok) {
    managerEmployees = (await managerEmployeesResponse.json())
      .map((employee) => ({
        id: employee.Key,
        name: employee.Name,
      }));
    fetchClientsManagerEmployees.className = 'text-success';
    console.log('Manager Employees: OK');
  } else {
    fetchClientsManagerEmployees.className = 'text-danger';
    console.log('Manager Employees: NOT OK');
  }

  if (timeTrackerClientsResponse.ok && timeTrackerUsersResponse.ok && managerCustomersResponse.ok && managerEmployeesResponse.ok) {
    console.log(`${timeTrackerClients.length} ${selections.timeTracker} Clients`, timeTrackerClients);
    console.log(`${timeTrackerUsers.length} ${selections.timeTracker} Users`, timeTrackerUsers);
    console.log(`${managerCustomers.length} Manager Customers`, managerCustomers);
    console.log(`${managerEmployees.length} Manager Employees`, managerEmployees);
    populateSelectWithOptions(timeTrackerClientIdInput, timeTrackerClients);
    populateSelectWithOptions(managerCustomerIdInput, managerCustomers);
    populateSelectWithOptions(timeTrackerUserIdInput, timeTrackerUsers);
    populateSelectWithOptions(managerEmployeeIdInput, managerEmployees);
    setTab(4);
  } else {
    fetchClientsSubmit.innerText = 'Back';
    fetchClientsError.innerText = 'Could not complete your request, please try again or seek support.';
  }
})

importReportTab.addEventListener('submit', async function(event) {
  event.preventDefault();
  setTab(5);
  const formData = new FormData(event.target);
  for (const [key, value] of formData) {
    selections[key] = value;
  }
  fetchReportTimeTracker.className = 'text-warning';
  fetchReportSubmit.innerText = 'Stop';
  fetchReportError.innerText = '';
  generateSubmit.disabled = false;

  abortController = new AbortController()
  const signal = abortController.signal

  switch (selections.reportType) {
    case 'client':
      if (!selections.timeTrackerClientId || !selections.managerCustomerId) {
        fetchReportSubmit.innerText = 'Back';
        fetchReportError.innerText = 'Both Clockify Client and Manager Customer are required.';
        return;
      }
      timeTrackerReportResponse = await timeTracker.getClientReport(selections.timeTrackerWorkspaceId, selections.timeTrackerClientId, selections.reportStartDate, selections.reportEndDate, {
        signal,
      });
      break;
    
    case 'employee':
      if (!selections.timeTrackerUserId || !selections.managerEmployeeId) {
        fetchReportSubmit.innerText = 'Back';
        fetchReportError.innerText = 'Both Clockify User and Manager Employee are required.';
        return;
      }
      timeTrackerReportResponse = await timeTracker.getEmployeeReport(selections.timeTrackerWorkspaceId, selections.timeTrackerUserId, selections.reportStartDate, selections.reportEndDate, {
        signal,
      });
      break;
      
    default:
      throw new Error(`Unknown reportType "${selections.reportType}"`);
  }

  if (timeTrackerReportResponse.ok) {
    fetchReportTimeTracker.className = 'text-success';
    console.log(selections.timeTracker + ' Report: OK');
  } else {
    fetchReportTimeTracker.className = 'text-danger';
    console.log(selections.timeTracker + ' Report: NOT OK');
  }

  if (timeTrackerReportResponse.ok) {
    timeTrackerReport = timeTrackerReportResponse.report();
    console.log(`${selections.timeTracker} Report`, timeTrackerReport);
    while (generatePreview.firstChild) {
      generatePreview.removeChild(generatePreview.firstChild);
    }
    const totalGrandView = document.createElement('div');
    const totalGrandHours = hours(timeTrackerReport.total_grand);
    const totalGrandMinutes = minutes(timeTrackerReport.total_grand);
    const totalGrandViewStrong = document.createElement('strong');
    totalGrandViewStrong.innerText = 'Total';
    totalGrandView.appendChild(totalGrandViewStrong);
    const totalGrandViewSpan = document.createElement('span');
    totalGrandViewSpan.innerText = ` ${totalGrandHours}h ${totalGrandMinutes} min`;
    totalGrandView.appendChild(totalGrandViewSpan);
    generatePreview.appendChild(totalGrandView);
    
    if (timeTrackerReport.total_billable) {
      const totalBillableView = document.createElement('div');
      const totalBillableHours = hours(timeTrackerReport.total_billable);
      const totalBillableMinutes = minutes(timeTrackerReport.total_billable);
      const totalBillableViewStrong = document.createElement('strong');
      totalBillableViewStrong.innerText = 'Total Billable';
      totalBillableView.appendChild(totalBillableViewStrong);
      const totalBillableViewSpan = document.createElement('span');
      totalBillableViewSpan.innerText = ` ${totalBillableHours}h ${totalBillableMinutes} min`;
      totalBillableView.appendChild(totalBillableViewSpan);
      generatePreview.appendChild(totalBillableView);
    }

    timeTrackerReport.total_currencies.forEach((total) => {
      if (total.amount !== null && total.currency !== null) {
        const currencyView = document.createElement('div');
        const currencyViewStrong = document.createElement('strong');
        currencyViewStrong.innerText = 'Total';
        currencyView.appendChild(currencyViewStrong);
        const currencyViewSpan = document.createElement('span');
        currencyViewSpan.innerText = ` ${total.amount} ${total.currency}`;
        currencyView.appendChild(currencyViewSpan);
        generatePreview.appendChild(currencyView);
      }
    });

    generatePreview.appendChild(document.createElement('br'));
    generatePreview.appendChild(document.createElement('hr'));
    generatePreview.appendChild(document.createElement('br'));

    switch (selections.reportType) {
      case 'client':
        managerBillableTime = null
        if (timeTrackerReport.data.length > 0) {
          managerBillableTime = timeTrackerReport.data.map(group => (
            group.items.map(item => ({
              Date: today(),
              Description: `${selections.reportStartDate} - ${selections.reportEndDate} - ${group.title.project} - ${item.title.user}${item.cur !== null ? ' - ' + item.cur : ''}`,
              TimeSpent: hours(item.time),
              TimeSpentMinutes: minutes(item.time),
              HourlyRate: item.rate || 0,
              CustomerID: selections.managerCustomerId,
            }))
          )).reduce((r, a) => r.concat(a), [])

          const billableTimeDiv = document.createElement('p');
          billableTimeDiv.className = 'text-muted';
          billableTimeDiv.innerText = `Date | Description | Time spent | Hourly rate`;
          generatePreview.appendChild(billableTimeDiv);
          managerBillableTime.forEach((billableTime) => {
            const billableTimeDiv = document.createElement('p');
            billableTimeDiv.className = 'text-muted';
            billableTimeDiv.innerText = `${billableTime.Date} | ${billableTime.Description} | ${billableTime.TimeSpent}h ${billableTime.TimeSpentMinutes}m | ${billableTime.HourlyRate}`;
            generatePreview.appendChild(billableTimeDiv);
          });
          console.log('Manager Billable Time', managerBillableTime)
        } else {
          const emptyDiv = document.createElement('p');
          emptyDiv.className = 'text-danger';
          emptyDiv.innerText = `There are no ${selections.timeTracker} billable time entries found based on your selections, please try something else.`;
          generatePreview.appendChild(emptyDiv);
        }
        break;
        
      case 'employee':
        managerPayslip = null
        if (timeTrackerReport.data.length > 0) {
          managerPayslip = {
            Date: today(),
            Description: `${selections.reportStartDate} - ${selections.reportEndDate}`,
            Earnings: timeTrackerReport.data.map(group => ({
              Units: 1,
              UnitPrice: group.total_currencies[0].amount || 0,
              Description: `${group.title.client || 'Without client'} - ${hours(group.time)}h ${minutes(group.time)}min${group.total_currencies[0].currency !== null ? ' - ' + group.total_currencies[0].currency : ''}`,
            })),
            Deductions: [
              {}
            ],
            Contributions: [
              {}
            ],
            Employee: selections.managerEmployeeId,
          }

          const payslipDiv = document.createElement('p');
          payslipDiv.className = 'text-muted';
          payslipDiv.innerText = `${managerPayslip.Date} - ${managerPayslip.Description}`;
          generatePreview.appendChild(payslipDiv);
          managerPayslip.Earnings.forEach((earning) => {
            const earningDiv = document.createElement('p');
            earningDiv.className = 'text-muted';
            earningDiv.innerText = `${earning.Description} | ${earning.Units} | ${earning.UnitPrice}`;
            generatePreview.appendChild(earningDiv);
          });
          console.log('Manager Payslip', managerPayslip)
        } else {
          const emptyDiv = document.createElement('p');
          emptyDiv.className = 'text-danger';
          emptyDiv.innerText = `There are no ${selections.timeTracker} billable time entries found based on your selections, please try something else.`;
          generatePreview.appendChild(emptyDiv);
        }
        break;
        
      default:
        throw new Error(`Unknown reportType "${selections.reportType}"`);
    }

    generateSubmit.disabled = timeTrackerReport.data.length === 0

    setTab(6);
  } else {
    fetchReportSubmit.innerText = 'Back';
    fetchReportError.innerText = 'Could not complete your request, please try again or seek support.';
  }
})

generateTab.addEventListener('submit', async function(event) {
  event.preventDefault();

  generateSubmit.disabled = true;
  generateBack.innerText = 'Stop';
  generateError.innerText = '';

  abortController = new AbortController()
  const signal = abortController.signal

  switch (selections.reportType) {
    case 'client':
      const managerBillableTimeReponse = await Promise.all(managerBillableTime.map((billableTimeEntry) => (
        fetch(`${managerUrl}/api/${selections.managerBusinessId}/${managerBillableTimeCollectionGuid}`, {
          method: 'POST',
          headers: {
            Authorization: 'Basic ' + btoa(`${managerUsername}:${managerPassword}`),
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(billableTimeEntry),
          signal,
        })
      )));
      const managerBillableTimeReponseOk = managerBillableTimeReponse.every(res => res.ok);
      
      if (managerBillableTimeReponseOk) {
        refreshAllTabsMatching('/billable-time-entries');
        successManagerEntity.innerText = 'Manager Billable Time';
        successTimeTrackerEntity.innerText = 'Report';
        successLink.href = managerUrl + `/billable-time-entries?FileID=${selections.managerBusinessId}`;
        successLink.innerText = 'Billable Time'
        setTab(7);
      } else {
        generateBack.innerText = 'Back';
        generateError.innerText = 'Could not complete your request, please try again or seek support.';
      }
      break;

    case 'employee':
      const managerPayslipReponse = await fetch(`${managerUrl}/api/${selections.managerBusinessId}/${managerPayslipCollectionGuid}`, {
        method: 'POST',
        headers: {
          Authorization: 'Basic ' + btoa(`${managerUsername}:${managerPassword}`),
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(managerPayslip),
        signal,
      });
      
      if (managerPayslipReponse.ok) {
        refreshAllTabsMatching('/payslips');
        successManagerEntity.innerText = 'Manager Payslip';
        successTimeTrackerEntity.innerText = 'Report';
        successLink.href = managerUrl + `/payslips?FileID=${selections.managerBusinessId}`;
        successLink.innerText = 'Payslips'
        setTab(7);
      } else {
        generateBack.innerText = 'Back';
        generateError.innerText = 'Could not complete your request, please try again or seek support.';
      }
      break;
    
    default:
      throw new Error(`Unknown reportType "${selections.reportType}"`)
  }

  generateSubmit.disabled = false;
});

fetchNamespacesTab.addEventListener('submit', function(event) {
  event.preventDefault();
  setTab(0);
  fetchNamespacesSubmit.innerText = 'Back';
  abortController.abort();
});

chooseNamespacesBack.addEventListener('click', function(event) {
  event.preventDefault();
  setTab(0);
});

fetchClientsTab.addEventListener('submit', function(event) {
  event.preventDefault();
  setTab(2);
  fetchClientsSubmit.innerText = 'Back';
  abortController.abort();
});

importReportBack.addEventListener('click', function(event) {
  event.preventDefault();
  setTab(2);
});

fetchReportTab.addEventListener('submit', function(event) {
  event.preventDefault();
  setTab(4);
  fetchReportSubmit.innerText = 'Back';
  abortController.abort();
});

generateBack.addEventListener('click', function(event) {
  event.preventDefault();
  setTab(4);
  generateBack.innerText = 'Back';
  abortController.abort();
});

const tabs = document.querySelectorAll('.tabs > .tab');
function setTab(tabIndex) {
  for (let i = 0, tab; tab = tabs[i]; i++) {
    tab.style.transform = `translateX(${(i - tabIndex) * 100}%)`;
    tab.style.display = '';
  }
  tabs[tabIndex].style.display = 'block';
}
setTab(0);
