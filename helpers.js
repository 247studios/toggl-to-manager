function populateSelectWithOptions(select, options) {
  while (select.firstChild) {
    select.removeChild(select.firstChild);
  }
  options.forEach(option => {
    const optionEl = document.createElement('option');
    optionEl.innerText = option.name;
    optionEl.value = option.id;
    select.appendChild(optionEl);
  })
}

function hours(milliseconds) {
  return Math.floor(milliseconds / 1000 / 60 / 60);
}

function minutes(milliseconds) {
  return Math.floor(((milliseconds / 1000 / 60 / 60) - Math.floor(milliseconds / 1000 / 60 / 60)) * 60);
}

function toQueryString(obj) {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}

function refreshAllTabsMatching(pathname) {
  chrome.tabs.query({}, function(tabs) {
    tabs.forEach(tab => {
      try {
        const url = new URL(tab.url);
        const managerHost = new URL(managerUrl).host;
        if (managerHost === url.host && url.pathname === pathname) {
          chrome.tabs.update(tab.id, {
            url: tab.url,
          });
        }
      } catch (err) {
        console.error(tab, err);
      }
    });
  });
}

function today() {
  return new Date().toISOString().substr(0, 10);
}

function roundTo(number, to) {
  const place = Math.pow(10, to);
  return Math.round(number * place) / place
}