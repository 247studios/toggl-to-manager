function Clockify() {
  this.apiKey = null;
  this.baseUrl = 'https://api.clockify.me'
  this.reportsBaseUrl = 'https://reports.api.clockify.me'
}

Clockify.prototype.getWorkspaces = async function getWorkspaces(options) {
  const response = await fetch(`${this.baseUrl}/api/v1/workspaces/`, {
    headers: this.getAuthorizationHeader(),
    ...options,
  });
  const body = await response.json()
  response.workspaces = function() {
    return body
      .map(workspace => ({
        id: workspace.id,
        name: workspace.name,
        currency: workspace.hourlyRate.currency,
      }));
  }
  return response;
}

Clockify.prototype.getClients = async function getClients(workspaceId, options) {
  const response = await fetch(`${this.baseUrl}/api/v1/workspaces/${workspaceId}/clients`, {
    headers: this.getAuthorizationHeader(),
    ...options,
  });
  const body = await response.json();
  response.clients = function() {
    return body.map(client => ({
      id: client.id,
      name: client.name,
    }));
  }
  return response;
}

Clockify.prototype.getUsers = async function getUsers(workspaceId, options) {
  const response = await fetch(`${this.baseUrl}/api/v1/workspaces/${workspaceId}/users`, {
    headers: this.getAuthorizationHeader(),
    ...options,
  });
  const body = await response.json();
  response.users = function() {
    return body
      .filter(({name}) => Boolean(name))
      .map(user => ({
        id: user.id,
        name: user.name,
      }));
  }
  return response;
}

/**
 * interface Report {
 *   "total_grand": Number, // 123 (ms)
 *   "total_billable": Number, // 123 (ms)
 *   "total_currencies": Array<{
 *     "amount": Number, // 1.23 ($)
 *     "currency": String, // "USD"
 *   }>,
 *   "data": Array<{
 *     "title": {
 *       "project": String, // "Galaxy"
 *     },
 *     "time": Number, // 123 (ms)
 *     "items": Array<{
 *       "title": {
 *         "user": String, // "John"
 *       },
 *       "cur": String, // "USD"
 *       "time": Number, // 123 (ms)
 *       "rate": Number, // 1.23 ($)
 *     }>,
 *     "total_currencies": Array<{
 *       "amount": Number, // 1.23 ($)
 *       "currency": String, // USD
 *     }>,
 *   }>,
 * }
 */

Clockify.prototype.getClientReport = async function getReport(workspaceId, clientId, startDate, endDate, options) {
  const workspaceResponse = await this.getWorkspaces();
  if (!workspaceResponse.ok) {
    return workspaceResponse;
  }
  const workspace = workspaceResponse.workspaces().find(({id}) => id === workspaceId);
  const currency = workspace.currency;
  const response = await fetch(`${this.reportsBaseUrl}/v1/workspaces/${workspaceId}/reports/summary`, {
    method: 'POST',
    headers: {
      ...this.getAuthorizationHeader(),
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "dateRangeStart": startDate + 'T00:00:00.000Z',
      "dateRangeEnd": endDate + 'T23:59:59.999Z',
      "rounding": false,
      "billable": true,
      "summaryFilter": {
        "groups": [
          "CLIENT",
          "USER"
        ],
      },
      "clients": {
        "ids": [clientId],
        "contains": "CONTAINS",
        "status": "ALL"
      },
    }),
    ...options,
  });
  const body = await response.json();
  response.report = function () {
    return {
      total_grand: moment.duration(body.totals.reduce((totalTime, total) => totalTime + total.totalTime, 0), 'seconds').asMilliseconds(),
      total_billable: moment.duration(body.totals.reduce((totalTime, total) => totalTime + total.totalBillableTime, 0), 'seconds').asMilliseconds(),
      total_currencies: [{
        amount: roundTo(body.totals.reduce((totalAmount, total) => totalAmount + total.totalAmount, 0)/ 100, 2),
        currency,
      }],
      data: body.groupOne.map((clientGroup) => ({
        title: {
          project: clientGroup.name,
        },
        time: roundTo(moment.duration(clientGroup.duration, 'seconds').asMilliseconds(), 2),
        items: clientGroup.children.map((userGroup) => ({
          title: {
            user: userGroup.name,
          },
          cur: currency,
          time: roundTo(moment.duration(userGroup.duration, 'seconds').asMilliseconds(), 2),
          rate: roundTo(userGroup.amount / moment.duration(userGroup.duration, 'seconds').asHours() / 100, 2),
        })),
        total_currencies: [{
          amount: roundTo(clientGroup.amount / 100, 2),
          currency,
        }],
      })),
    };
  }
  return response;
}

Clockify.prototype.getEmployeeReport = async function getReport(workspaceId, userId, startDate, endDate, options) {
  const workspaceResponse = await this.getWorkspaces();
  if (!workspaceResponse.ok) {
    return workspaceResponse;
  }
  const workspace = workspaceResponse.workspaces().find(({id}) => id === workspaceId);
  const currency = workspace.currency;
  const response = await fetch(`${this.reportsBaseUrl}/v1/workspaces/${workspaceId}/reports/summary`, {
    method: 'POST',
    headers: {
      ...this.getAuthorizationHeader(),
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "dateRangeStart": startDate + 'T00:00:00.000Z',
      "dateRangeEnd": endDate + 'T23:59:59.999Z',
      "rounding": false,
      "billable": true,
      "summaryFilter": {
        "groups": [
          "CLIENT"
        ],
      },
      "users": {
        "ids": [userId],
        "contains": "CONTAINS",
        "status": "ALL"
      },
    }),
    ...options,
  });
  const body = await response.json();
  response.report = function () {
    return {
      total_grand: moment.duration(body.totals.reduce((totalTime, total) => totalTime + total.totalTime, 0), 'seconds').asMilliseconds(),
      total_billable: moment.duration(body.totals.reduce((totalTime, total) => totalTime + total.totalBillableTime, 0), 'seconds').asMilliseconds(),
      total_currencies: [{
        amount: roundTo(body.totals.reduce((totalAmount, total) => totalAmount + total.totalAmount, 0) / 100, 2),
        currency,
      }],
      data: body.groupOne.map((clientGroup) => ({
        title: {
          client: clientGroup.name,
        },
        time: roundTo(moment.duration(clientGroup.duration, 'seconds').asMilliseconds(), 2),
        items: [],
        total_currencies: [{
          amount: roundTo(clientGroup.amount / 100, 2),
          currency,
        }],
      })),
    };
  }
  return response;
}

Clockify.prototype.getAuthorizationHeader = function getAuthorizationHeader() {
  return {
    'X-Api-Key': this.apiKey,
  }
}

Clockify.prototype.setApiKey = function setApiKey(apiKey) {
  this.apiKey = apiKey;
}
