function Toggl() {
  this.apiKey = null;
  this.baseUrl = 'https://api.track.toggl.com'
  this.userAgent = 'user_agent=omar%40247studios.me'
}

Toggl.prototype.getWorkspaces = async function getWorkspaces(options) {
  const response = await fetch(`${this.baseUrl}/api/v8/workspaces?${this.userAgent}`, {
    headers: this.getAuthorizationHeader(),
    ...options,
  });
  const body = await response.json()
  response.workspaces = function() {
    return body
      .map(workspace => ({
        id: workspace.id,
        name: workspace.name,
      }));
  }
  return response;
}

Toggl.prototype.getClients = async function getClients(workspaceId, options) {
  const response = await fetch(`${this.baseUrl}/api/v8/workspaces/${workspaceId}/clients?${this.userAgent}`, {
    headers: this.getAuthorizationHeader(),
    ...options,
  });
  const body = await response.json();
  response.clients = function() {
    return body.map(client => ({
      id: client.id,
      name: client.name,
    }));
  }
  return response;
}

Toggl.prototype.getUsers = async function getUsers(workspaceId, options) {
  const response = await fetch(`${this.baseUrl}/api/v8/workspaces/${workspaceId}/workspace_users?${this.userAgent}`, {
    headers: this.getAuthorizationHeader(),
    ...options,
  });
  const body = await response.json();
  response.users = function() {
    return body.map(user => ({
      id: user.uid,
      name: user.name,
    }));
  }
  return response;
}

Toggl.prototype.getClientReport = async function getReport(workspaceId, clientId, startDate, endDate, options) {
  const togglReportParams = toQueryString({
    grouping: 'projects',
    subgrouping: 'users',
    order_field: 'title',
    order_desc: 'off',
    rounding: 'Off',
    distinct_rates: 'On',
    billable: 'yes',
    status: 'active',
    client_ids: clientId,
    calculate: 'time',
    sortDirection: 'asc',
    sortBy: 'title',
    page: '1',
    since: startDate,
    until: endDate,
    with_total_currencies: '1',
    subgrouping_ids: 'true',
    date_format: 'YYYY-MM-DD',
  });
  const response = await fetch(`${this.baseUrl}/reports/api/v2/summary?${this.userAgent}&workspace_id=${workspaceId}&${togglReportParams}`, {
    headers: this.getAuthorizationHeader(),
    ...options,
  });
  const body = await response.json();
  response.report = function () {
    return {
      total_grand: body.total_grand,
      total_billable: body.total_billable,
      total_currencies: body.total_currencies.map(cur => ({
        amount: cur.amount,
        currency: cur.currency,
      })),
      data: body.data.map(group => ({
        title: {
          project: group.title.project,
        },
        time: group.time,
        items: group.items.map(item => ({
          title: {
            user: item.title.user,
          },
          cur: item.cur,
          time: item.time,
          rate: item.rate,
        })),
        total_currencies: group.total_currencies.map(cur => ({
          amount: cur.amount,
          currency: cur.currency,
        })),
      })),
    }
  }
  return response;
}

Toggl.prototype.getEmployeeReport = async function getReport(workspaceId, userId, startDate, endDate, options) {
  const togglReportParams = toQueryString({
    grouping: 'clients',
    subgrouping: 'time_entries',
    order_field: 'title',
    order_desc: 'off',
    rounding: 'Off',
    distinct_rates: 'On',
    billable: 'yes',
    status: 'active',
    user_ids: userId,
    calculate: 'time',
    sortDirection: 'asc',
    sortBy: 'title',
    page: '1',
    since: startDate,
    until: endDate,
    with_total_currencies: '1',
    subgrouping_ids: 'true',
    date_format: 'YYYY-MM-DD',
  });
  const response = await fetch(`${this.baseUrl}/reports/api/v2/summary?${this.userAgent}&workspace_id=${workspaceId}&${togglReportParams}`, {
    headers: this.getAuthorizationHeader(),
    ...options,
  });
  const body = await response.json();
  response.report = function () {
    return {
      total_grand: body.total_grand,
      total_billable: body.total_billable,
      total_currencies: body.total_currencies.map(cur => ({
        amount: cur.amount,
        currency: cur.currency,
      })),
      data: body.data.map(group => ({
        title: {
          client: group.title.client,
        },
        time: group.time,
        items: group.items.map(item => ({
          title: {
            user: item.title.user,
          },
          cur: item.cur,
          time: item.time,
          rate: item.rate,
        })),
        total_currencies: group.total_currencies.map(cur => ({
          amount: cur.amount,
          currency: cur.currency,
        })),
      })),
    }
  }
  return response;
}

Toggl.prototype.getAuthorizationHeader = function getAuthorizationHeader() {
  return {
    Authorization: 'Basic ' + btoa(`${this.apiKey}:api_token`)
  }
}

Toggl.prototype.setApiKey = function setApiKey(apiKey) {
  this.apiKey = apiKey;
}